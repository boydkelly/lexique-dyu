#!/usr/bin/bash
git commit vocabulaire.yaml -m "before yaml_sort" --quiet
echo "---" > tmp.yaml
yq < vocabulaire.yaml | sed '$d' | yq sort -y >> tmp.yaml
echo "---" >> tmp.yaml
if [[ yq v tmp.yaml; ]] ; then mv -v tmp.yaml vocabulaire.yaml
git commit vocabulaire.yaml -m "after yaml_sort" --quiet

