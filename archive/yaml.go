package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

type instanceConfig struct {
	Name    string `yaml:"Name"`
	Address string `yaml:"Address"`
	Phone   int    `yaml:"Phone"`
	WFH     bool
}

func (c *instanceConfig) Parse(data []byte) error {
	return yaml.Unmarshal(data, c)
}

func main() {
	data, err := ioutil.ReadFile("data.yaml")
	if err != nil {
		log.Fatal(err)
	}
	var config instanceConfig
	if err := config.Parse(data); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", config)
}
