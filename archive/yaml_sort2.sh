#!/usr/bin/bash
yq < data.yaml | jq '.items |= sort_by(.)' | yq -y
