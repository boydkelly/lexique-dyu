<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<xsl:key name="initialChar" match="resource" use="translate(substring(name,1,1),
                                                     'abcdefghijklmnopqrstuvwxyz',
                                                  'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />

<xsl:template match="/">
    <html>
    <body>
    <!-- iterate over all the unique initial letter values -->
    <xsl:for-each select="//resource[generate-id(.)=generate-id(key('initialChar', 
                                                                  translate(substring(name,1,1),
                                                                'abcdefghijklmnopqrstuvwxyz',
                                                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))[1])]">
        <!-- sort the nodes for this initial letter value by name -->
        <xsl:sort select="name"/>
          <xsl:variable name="myChar" select="translate(substring(name,1,1),
                                                      'abcdefghijklmnopqrstuvwxyz',
                                                         'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
        <!-- output the links -->
        <xsl:call-template name="genLinks"/>
        <xsl:element name="a"><xsl:attribute name="name"><xsl:value-of select="string($myChar)"/></xsl:attribute></xsl:element>
        <!-- output all the nodes having this intial letter value -->
        <xsl:for-each select="key('initialChar',$myChar)">
            <xsl:sort select="name"/>
            <h2><xsl:value-of select="name"/></h2>
            <h3><xsl:value-of select="description"/></h3>
        </xsl:for-each>
      </xsl:for-each>
        <!-- output the final links -->
        <xsl:call-template name="genLinks"/>
    </body>
    </html>

</xsl:template>

<xsl:template name="genLinks">
        <span style="letter-spacing:7px; font-size=12pt">
        <center>
        <!-- The for-each will iterate exactly once for each unique initial letter. -->
        <xsl:for-each select="//resource[generate-id(.)=generate-id(key('initialChar', 
                                                                       translate(substring(name,1,1),
                                                                    'abcdefghijklmnopqrstuvwxyz',
                                                                    'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))[1])]">
            <xsl:sort select="name"/>
            <xsl:variable name="myLinkChar" select="translate(substring(name,1,1),
                                            'abcdefghijklmnopqrstuvwxyz',
                                            'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
            <a><xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="$myLinkChar"/></xsl:attribute>
               <xsl:value-of select="$myLinkChar"/></a>
               <xsl:if test="position()!=last()"><xsl:text>-</xsl:text></xsl:if>
        </xsl:for-each>
        </center>
        </span>
</xsl:template>

</xsl:stylesheet>
