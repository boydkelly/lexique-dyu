#!/usr/bin/bash
 awk -F"\t" '
($3 in a) {          # look for duplicates in $2
    if(a[$3]) {      # if found
        print a[$3]  # output the first, stored one
        a[$3]=""     # mark it outputed
    }
    print NR,$0      # print the duplicated one
    next             # skip the storing part that follows
}
{
    a[$3]=NR OFS $0  # store the first of each with NR and full record
}' headwords-6.tsv

