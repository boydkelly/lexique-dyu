<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
  xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs   = "http://www.w3.org/2001/XMLSchema"
  xmlns:c    = "http://www.coastsystems.net">


  <!-- output tsv file for anki examples-->
  <xsl:output method="text" encoding="UTF-8" />

  <xsl:param name="header" select="true()" />
  <xsl:param name="pipe" select="'|'" />
  <xsl:param name="CSEP" select="'&#09;'" />
  <xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->


  <xsl:template match="/">
    <!-- speech: the main elements -->
    <xsl:variable name="headwords" select="c:lexique/c:headword" />
    <xsl:variable name="defs" select="$headwords/c:trans/c:speech/c:def" />
    <xsl:variable name="speech" select="$headwords/c:trans/c:speech" />

    <!-- Determine the maximal number of <alt> and <example> elements -->

    <xsl:variable name="exampleCount">
      <xsl:for-each select="$speech">
        <count count="{count(c:example)}" />
      </xsl:for-each>
    </xsl:variable>

        <!-- <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer" />-->
    <xsl:variable name="maxExamples" select="18 cast as xs:integer" />

    <!-- Header. -->
    <xsl:if test="$header">
        <xsl:text>guid</xsl:text>
        <xsl:copy-of select="$CSEP" />
        <xsl:text>hint1</xsl:text>
        <xsl:copy-of select="$CSEP" />
        <xsl:text>dyu</xsl:text>
        <xsl:copy-of select="$CSEP" />
        <xsl:text>french</xsl:text>
        <xsl:copy-of select="$CSEP" />
        <xsl:text>tags</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>

    <!-- Data -->
    <xsl:for-each select="$speech">
      <xsl:sort select="ancestor::c:headword/c:dyu" lang="en" />
          <xsl:variable name="pos" select="c:type" />

          <!-- duy -->
          <xsl:variable name="examples" select="c:def/c:example" />
          <xsl:for-each select="$examples">
            <xsl:if test="c:source != ''">
              <xsl:value-of select="normalize-space(c:ex-id)" />
              <xsl:copy-of select="$CSEP" />

              <xsl:choose>
                <xsl:when test="$pos='composé'">
                  <xsl:value-of select="ancestor::c:speech/c:def/c:note2" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="ancestor::c:headword/c:dyu" />
                </xsl:otherwise>
              </xsl:choose>

              <xsl:copy-of select="' = '" />
              <xsl:value-of select="ancestor::c:speech/c:def/c:gloss" />
              <xsl:copy-of select="$CSEP" />
              <xsl:value-of select="normalize-space(c:source)" />
              <xsl:copy-of select="$CSEP" />
              <xsl:value-of select="normalize-space(c:target)" />
              <xsl:copy-of select="$CSEP" />
              <xsl:copy-of select="$RSEP" />
            </xsl:if>
          </xsl:for-each>

        </xsl:for-each>
      </xsl:template>

    </xsl:stylesheet>

