<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
  xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs   = "http://www.w3.org/2001/XMLSchema"
  xmlns:c    = "http://www.coastsystems.net">

<xsl:strip-space elements="c:note2" />

  <!-- output tsv file for anki -->
  <xsl:output method="text" encoding="UTF-8" />

  <xsl:param name="header" select="true()" />
  <xsl:param name="pipe" select="'|'" />
  <xsl:param name="CSEP" select="'&#09;'" />
  <xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

  <xsl:template match="/">
    <!-- speech: the main elements -->
    <xsl:variable name="headwords" select="c:lexique/c:headword" />
    <xsl:variable name="defs" select="$headwords/c:trans/c:speech/c:def" />
    <xsl:variable name="speech" select="$headwords/c:trans/c:speech" />

    <!-- Determine the maximal number of <alt> and <example> elements -->
    <xsl:variable name="altCount">
      <xsl:for-each select="$headwords">
        <count count="{count(c:alt)}" />
      </xsl:for-each>
    </xsl:variable>
    <!-- hard code this to 4 <xsl:variable name="maxAlts" select="max($altCount/count/@count) cast as xs:integer" />-->
    <xsl:variable name="maxAlts" select="4 cast as xs:integer" />

    <xsl:variable name="cfCount">
      <xsl:for-each select="$headwords">
        <!-- <count count="{count(c:cf)}" /> -->
        <count count="4"/>
      </xsl:for-each>
    </xsl:variable>
    <!--    <xsl:variable name="maxCfs" select="max($cfCount/count/@count) cast as xs:integer" /> -->
    <xsl:variable name="maxCfs" select="4 cast as xs:integer" />

    <xsl:variable name="exampleCount">
      <xsl:for-each select="$speech">
        <count count="{count(c:def/c:example/c:source)}" />
      </xsl:for-each>
    </xsl:variable>
        <!-- <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer" />-->
    <xsl:variable name="maxExamples" select="18 cast as xs:integer" />

    <!-- Header. -->
    <xsl:if test="$header">
      <!-- dyu -->
      <xsl:text>guid</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>dyu</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>source</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>speech</xsl:text>
      <!-- gloss -->
      <xsl:copy-of select="$CSEP" />
      <xsl:text>french</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>hint1</xsl:text>

      <!-- alt1..altN -->
      <xsl:for-each select="(1 to $maxAlts)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>alt</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>

      <!-- emp -->
      <xsl:copy-of select="$CSEP" />
        <xsl:text>emp</xsl:text>

      <xsl:for-each select="(1 to $maxCfs)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>cf</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>

      <!-- notes -->
      <xsl:for-each select="(1 to $maxAlts)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>note</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>

      <!-- anki text2,3,4 (not used here) -->
      <xsl:for-each select="(2 to $maxAlts)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>text</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>

      <!-- anki examples (not used here) -->
      <xsl:for-each select="(1 to $maxAlts)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>example</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>

      <!-- source1,target1..sourceN,targetN -->
      <xsl:for-each select="(1 to $maxExamples)">
        <xsl:copy-of select="$CSEP" />
        <xsl:text>source</xsl:text>
        <xsl:value-of select="." />
        <xsl:copy-of select="$CSEP" />
        <xsl:text>target</xsl:text>
        <xsl:value-of select="." />
      </xsl:for-each>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>english</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>tags</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>

    <!-- Data -->
    <xsl:for-each select="$speech">
      <xsl:sort select="ancestor::c:headword/c:dyu" lang="en" />

     <xsl:variable name="pos" select="c:type" />
     <!-- dyu -->
     <xsl:value-of select="c:t-uuid" />
     <xsl:copy-of select="$CSEP" />

     <xsl:choose>
       <xsl:when test="$pos='composé'">
         <!-- duy -->
         <xsl:value-of select="c:def/c:note2" />
       </xsl:when>
       <xsl:otherwise>
         <!-- duy -->
         <xsl:value-of select="ancestor::c:headword/c:dyu" />
       </xsl:otherwise>
     </xsl:choose>

     <!-- source -->
     <xsl:copy-of select="$CSEP" />
     <xsl:text></xsl:text>
     <!-- type/pos -->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="c:type" />
     <!-- gloss -->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="c:def/c:gloss" separator=", " />
     <!-- hint -->
     <xsl:copy-of select="$CSEP" />
     <xsl:text></xsl:text>

     <!-- alt1..altN -->
     <xsl:variable name="alts" select="ancestor::c:headword/c:alt" />
     <xsl:for-each select="$alts">
       <xsl:copy-of select="$CSEP" />
       <xsl:value-of select="." />
     </xsl:for-each>

     <!-- this is when the alts is less than 4 need to complete blank alts to maxalts-->
     <xsl:for-each select="((count($alts) +1 ) cast as xs:integer to $maxAlts)">
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
     </xsl:for-each>

     <!-- emp -->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="ancestor::c:headword/c:emp" />

     <!-- cf -->
     <xsl:variable name="cfs" select="ancestor::c:headword/c:cf" />
     <xsl:for-each select="$cfs">
       <xsl:copy-of select="$CSEP" />
       <xsl:value-of select="." />
     </xsl:for-each>

     <xsl:for-each select="((count($cfs) +1 ) cast as xs:integer to $maxCfs)">
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
     </xsl:for-each>

     <!-- note1 Eventually migrate all this to cf and leave anki alone -->
     <xsl:copy-of select="$CSEP" />
     <xsl:text></xsl:text>

     <!-- detail> note2 -->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="c:def/c:note2" />

     <!-- note3  -->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="c:def/c:note3" />

     <!-- note4  is using tags-->
     <xsl:copy-of select="$CSEP" />
     <xsl:value-of select="c:def/c:tags" />

     <!-- anki text 2,3,4 not used here-->
     <xsl:for-each select="((2)  cast as xs:integer to 4)">
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
     </xsl:for-each>

     <!-- anki example 1,2,3,4 not used here-->
     <xsl:for-each select="((1)  cast as xs:integer to 4)">
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
     </xsl:for-each>

     <!-- source1,target1..sourceN,targetN -->
     <xsl:variable name="examples" select="c:def/c:example" />

     <xsl:for-each select="$examples">
       <xsl:copy-of select="$CSEP" />
       <!-- Normalize space because the fields may have stray LFs. -->
       <xsl:value-of select="normalize-space(c:source)" />
       <xsl:copy-of select="$CSEP" />
       <xsl:value-of select="normalize-space(c:target)" />
     </xsl:for-each>

     <xsl:for-each select="((count($examples) + 1) cast as xs:integer to $maxExamples)">
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
       <xsl:copy-of select="$CSEP" />
       <xsl:text></xsl:text>
     </xsl:for-each>

     <!-- English, Tags -->
     <xsl:copy-of select="$CSEP" />
     <xsl:text></xsl:text>
     <xsl:copy-of select="$CSEP" />
     <xsl:text>hw5</xsl:text>
     <!--
       <xsl:text>"hw"</xsl:text>
     -->
       <xsl:copy-of select="$RSEP" />
     </xsl:for-each>
   </xsl:template>

 </xsl:stylesheet>

