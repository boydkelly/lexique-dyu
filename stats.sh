#!/usr/bin/bash
doc=lexique-dyu.adoc
hw=headwords.yaml

cp _lexique-dyu.adoc $doc
today=$(date -Im)
headwords=$(grep dyu: $hw | wc -l)
sub=$(grep composé $hw | wc -l)
examples=$(sed -n -r '/source: [^\s*]/p' $hw | wc -l)

echo $today,$headwords,$sub,$examples >> stats.csv

sed -i "s/x_date/$(LANG=en_CA date -u '+%d %B %Y')/" $doc 

sed -i "s/x_words/$headwords/" $doc

sed -i "s/x_sub/$sub/" $doc

sed -i "s/x_ex/$examples/" $doc
exit
