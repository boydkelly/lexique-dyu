#!/usr/bin/bash
cmd="uuidgen"
#
tsv="/var/home/bkelly/Anki/brainbrew/Jula/src/data/mois.tsv"
rm out.yaml 
grep -v phrase $tsv | awk -v cmd="$cmd" -v uuid="$($cmd)" -F '\t' 'BEGIN { out="out.yaml"; }
                 { uuid4 = ((cmd | getline xout) > 0 ? xout : uuid) }
                 { close(cmd) }
                 { print  "  - dyu: "$2 >> out}
                 { print  "    alt: " >> out}
                 { if ($7) 
                     print  "      - "$7 >> out;
                   else
                     print  "      - null" >> out;
                 }
                 { if ($8) print  "      - "$8 >> out}
                 { if ($9) print  "      - "$9 >> out}
                 { print  "    trans: " >> out}
                 { print  "      - lang: fr" >> out}
                 { print  "        detail: null" >> out}
                 { print  "        speech:" >> out}
                 { print  "          - type: null" >> out}
                 { print  "            t-uuid: " uuid4 >> out}
                 { print  "            def:" >> out}
                 { print  "              - gloss: "$5 >> out}
                 { print  "                note: "$20 >> out}
                 { print  "                example:" >> out}
                 { print  "                  - source:" >> out}
                 { print  "                    target:" >> out}

                 {if ($60) print "          ",  $60 >> out}
                 {if($62) print "english:", "[[" $62 "]]">> out}'


#                   cmd="uuidgen"
#
#awk -v cmd="$cmd" -v uuid="$($cmd)" '
#BEGIN {FS=OFS="\t"}
#$2 ~ /^[[:blank:]]*$/ {
#   $2 = ((cmd | getline out) > 0 ? out : uuid)
#   close(cmd)
#} 1' file | column -t
