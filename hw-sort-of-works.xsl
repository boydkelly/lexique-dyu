<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:coas="http://www.coastsystems.net"
xmlns:c="http://www.coastsystems.net"
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tdef" select="'|==='" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="right_bottom" select="'>.>| '" />
<xsl:param name="asterisk" select="'* '" />
<xsl:param name="level1" select="'== '" />
<xsl:param name="level2" select="'=== '" />
<xsl:param name="doublecolon" select="' ::'" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:param name="delim" select="','" />
<xsl:strip-space elements="*" />

<xsl:key name="initialChar" match="c:lexique/c:headword" use="translate(substring(c:dyu,1,1),
'aàábcdeɛfghijklmnoòóɔpqrstuvwxyz',
'AÀÁBCDEƐFGHIJKLMNOÒÓƆPQRSTUVWXYZ')" />

<xsl:template match="c:lexique">

  <!-- header -->
  <xsl:text>:leveloffset: -1</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:text>:page-partial:</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:text>[grid="rows",cols="15,10,20,25,30"]</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
  <xsl:text>|jula|pd|français|note|exemples</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />

  <!--
    #not neded for now
    genlinks not currently used; asciidoctor makes links in sections 
    <xsl:call-template name="genLinks"/>
  -->

    <xsl:value-of select="$newline" />
    <xsl:value-of select="$newline" />
    <!-- iterate over all the unique initial letter values -->
    <xsl:for-each select="//c:headword[generate-id(.)=generate-id(key('initialChar', 
    translate(substring(c:dyu,1,1),
    'aàábcdeɛfghijklmnoòóɔpqrstuvwxyz',
    'AÀÁBCDEƐFGHIJKLMNOÒÓƆPQRSTUVWXYZ'))[1])]">

    <!-- sort here -->
    <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending" />
    <xsl:variable name="myChar" select="translate(substring(c:dyu,1,1),
    'aàábcdeɛfghijklmnoòóɔpqrstuvwxyz',
    'AÀÁBCDEƐFGHIJKLMNOÒÓƆPQRSTUVWXYZ')" />

    <!-- output the level1 header  and start asciidoc table-->
    <xsl:value-of select="$newline" />
    <xsl:value-of select="concat($level2, string($myChar))" /> 
    <xsl:value-of select="$newline" />
    <xsl:value-of select="$newline" />
    <xsl:text>toc&#58;&#58;[]</xsl:text>

    <!--
      <xsl:text>&lt;&lt;here,Index&gt;&gt;</xsl:text>
      "<a href='#'><img class='icon' src='/images/icons/chevron-up.svg'></a>")"/>
    -->

      <xsl:value-of select="$newline" />
      <xsl:value-of select="$newline" />
      <xsl:text>[role="flex",grid="rows",cols="15,10,20,25,30"]</xsl:text>
      <xsl:value-of select="$newline" />
      <xsl:value-of select="$tdef" />
      <xsl:value-of select="$newline" />

      <!-- output all the nodes having this intial letter value -->
      <xsl:for-each select="key('initialChar',$myChar)">
        <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending"/>
        <xsl:if test="c:emp !=''">
        <xsl:value-of select="concat($asciidoctor, '*', c:dyu,'* ', '*_', c:emp, '_*'  )" />
        </xsl:if>
        <xsl:if test="c:emp =''">
        <xsl:value-of select="concat($asciidoctor, '*', c:dyu,'*' )" />
        </xsl:if>

        <xsl:if test="c:alt">
          <xsl:value-of select="$plus"/>
        </xsl:if>
        <xsl:value-of select="$newline" />

        <xsl:for-each select="c:alt">
          <xsl:value-of select="text()" />
          <xsl:value-of select="$plus"/>
          <xsl:value-of select="$newline" />
        </xsl:for-each>

        <xsl:if test="c:cf != ''">
          <xsl:text>cf: </xsl:text>

          <xsl:for-each select="c:cf">
            <xsl:value-of select="text()" />
            <xsl:if test="following-sibling::c:cf">
              <xsl:value-of select="$delim" />
            </xsl:if>

          </xsl:for-each>
        </xsl:if>

        <xsl:for-each select="c:trans">

          <xsl:value-of select="$newline" />

          <xsl:value-of select="c:detail" />

          <xsl:for-each select="c:speech">
            <xsl:value-of select="$separator"/>
            <xsl:if test="position() > 1 ">
          <!-- after the firt pos separator replaces the dyu-->
              <xsl:value-of select="$separator"/>
            </xsl:if>

     <xsl:variable name="pos" select="c:type" />



     <xsl:choose>
       <xsl:when test="$pos='composé'">

       </xsl:when>
       <xsl:otherwise>
            <xsl:value-of select="c:type"></xsl:value-of>
       </xsl:otherwise>
     </xsl:choose>

            <xsl:value-of select="$separator"/>


            <xsl:for-each select="c:def">
              <xsl:if test="position() = 1 ">
                <xsl:value-of select="c:gloss"></xsl:value-of>
                <xsl:value-of select="$separator" />
     <xsl:choose>
       <xsl:when test="$pos='composé'">
         <xsl:value-of select="concat('*', c:note2,'*' )" />
       </xsl:when>
       <xsl:otherwise>
                <xsl:value-of select="c:note3"></xsl:value-of>
       </xsl:otherwise>
     </xsl:choose>
              </xsl:if>

              <xsl:if test="position() > 1 ">
                <xsl:value-of select="$newline" />
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="c:gloss"></xsl:value-of>
                <xsl:value-of select="$separator"/>
     <xsl:choose>
       <xsl:when test="$pos='composé'">
         <xsl:value-of select="concat('*', c:note2,'*' )" />
       </xsl:when>
       <xsl:otherwise>
                <xsl:value-of select="c:note3"></xsl:value-of>
       </xsl:otherwise>
     </xsl:choose>
              </xsl:if>

              <xsl:for-each select="c:example">
                <xsl:value-of select="$newline" />

                <xsl:if test="position() = 1 ">
                  <xsl:value-of select="$asciidoctor"/>
                </xsl:if>

                <xsl:value-of select="c:source"></xsl:value-of>
                <xsl:if test="c:source != ''"> 
                  <xsl:value-of select="$doublecolon" />
                </xsl:if>
                <xsl:value-of select="$newline" />
                <xsl:value-of select="c:target"></xsl:value-of>

              </xsl:for-each>

              <xsl:value-of select="$newline" />

            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:value-of select="$newline" />

      </xsl:for-each>
      <xsl:value-of select="$tdef"/>
      <xsl:value-of select="$newline" />
    </xsl:for-each>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <xsl:template name="genLinks">
    <!-- The for-each will iterate exactly once for each unique initial letter. -->
    <xsl:for-each select="//c:headword[generate-id(.)=generate-id(key('initialChar', 
    translate(substring(c:dyu,1,1),
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))[1])]">
    <xsl:sort select="c:dyu"/>
    <xsl:variable name="myLinkChar" select="translate(substring(c:dyu,1,1),
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <a><xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="$myLinkChar"/></xsl:attribute>
      <xsl:value-of select="$myLinkChar"/></a>
    <xsl:if test="position()!=last()"><xsl:text>-</xsl:text></xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
<!--
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$newline" />
-->
