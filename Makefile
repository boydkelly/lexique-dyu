doc := "lexique-dyu.adoc"
dyu := "vocab-dyu.adoc"
fr := "francais.tsv"
module := "/home/bkelly/dev/web-docs/docs/fr/modules/ROOT"
date := $(shell date -Ih)

SHELL := /bin/bash
REQUIRED_BINS := jq xslt3 csvclean
all: check

check:
	@type jq

	$(foreach bin,$(REQUIRED_BINS),\
    $(if $(shell type $(bin) 2> /dev/null),$(info Found `$(bin)`),$(error Please install `$(bin)`)))

publish: build
	git -C $(module) checkout main
	git -C $(module) push origin
  
build:
	#git update-index --refresh
	git diff --quiet --exit-code || git commit -a -m "published on: $(date)"
	#git push origin main --quiet
	./headwords2xml.sh
	xslt3 -xsl:headwords-alpha.xsl -s:headwords.xml -o:$(dyu)
	xslt3 -xsl:headwords-4.xsl -s:headwords.xml -o:headwords-4.tsv
	xslt3 -xsl:hw-anki-words.xsl -s:headwords.xml -o:hw-anki-words.tsv
	xslt3 -xsl:hw-anki-phrases.xsl -s:headwords.xml -o:hw-anki-phrases.tsv
	xslt3 -xsl:hw-all.xsl -s:headwords.xml -o:hw-all.txt
	csvclean -n -v -t headwords-4.tsv
	csvclean -n -v -t hw-anki-words.tsv
	csvclean -n -v -t hw-anki-phrases.tsv
	asciidoctor $(dyu)
	git diff --quiet --exit-code || git commit -a -m "published on: $(date)" && git push origin

	git -C $(module) checkout main

	cp hw-anki-words.tsv ~/Documents/Jula/brainbrew/Jula/src/data/
	cp hw-anki-phrases.tsv ~/Documents/Jula/brainbrew/Examples/src/data/
	cp hw-anki-phrases.tsv ~/dev/julakan-build/
	cp hw-anki-phrases.tsv ~/Documents/Jula/slides-dyu/

	./stats.sh
	cp $(doc) $(module)/pages
	cp $(dyu) $(module)/partials/
	cp $(fr) $(module)/partials/

	#cp $(module)/partials/* $(module)/_include/
	git -C $(module) diff --quiet --exit-code || git -C $(module) commit -a -m "published on: $(date)"
	#git -C $(module) commit -a -m publish 
