<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:z="http://www.coastsystems.net"
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tdef" select="'|==='" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="doublecolon" select="' ::'" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:strip-space elements="*" />

<xsl:template match="z:lexique">
  <xsl:text>[grid="rows",cols="3,3,^2,^2,8"]</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
  <xsl:text>|jula|français|note|gram.|exemples</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:for-each select="z:item">
    <xsl:value-of select="$separator"/>
    <xsl:value-of select="z:jula"></xsl:value-of>

    <xsl:for-each select="z:french/z:translations">
      <xsl:value-of select="$separator"/>
      <xsl:if test="position() > 1 ">
        <xsl:value-of select="$separator"/>
      </xsl:if>
      <xsl:value-of select="z:definition"></xsl:value-of>
      <xsl:value-of select="$separator" />
      <xsl:value-of select="z:hint"></xsl:value-of>

      <xsl:for-each select="z:speech">
        <xsl:if test="position() > 1 ">
          <xsl:value-of select="$separator"/>
          <xsl:value-of select="$separator"/>
          <xsl:value-of select="$separator"/>
        </xsl:if>
        <xsl:value-of select="$separator" />
        <xsl:value-of select="z:type"></xsl:value-of>

        <xsl:for-each select="z:example">
          <xsl:value-of select="$newline" />
          <xsl:if test="position() = 1 ">
            <xsl:value-of select="$asciidoctor"/>
          </xsl:if>

          <xsl:value-of select="z:source"></xsl:value-of>
          <xsl:if test="z:source != ''"> 
            <xsl:value-of select="$doublecolon" />
          </xsl:if>
          <xsl:value-of select="$newline" />
          <xsl:value-of select="z:target"></xsl:value-of>

        </xsl:for-each>
        <xsl:value-of select="$newline" />

      </xsl:for-each>
    </xsl:for-each>

    <xsl:value-of select="$newline" />
  </xsl:for-each>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
</xsl:template>
</xsl:stylesheet>
