<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:coas="http://www.coastsystems.net"
xmlns:c="http://www.coastsystems.net"
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tdef" select="'|==='" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="right_bottom" select="'>.>| '" />
<xsl:param name="asterisk" select="'* '" />
<xsl:param name="level1" select="'== '" />
<xsl:param name="level2" select="'=== '" />
<xsl:param name="level3" select="'==== '" />
<xsl:param name="doublecolon" select="' ::'" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:param name="delim" select="','" />
<xsl:strip-space elements="*" />

<xsl:key name="initialChar" match="c:lexique/c:headword" use="translate(substring(c:dyu,1,1),
'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

<xsl:template match="c:lexique">
  <!-- iterate over all the unique initial letter values -->
  <xsl:for-each select="//c:headword[generate-id(.)=generate-id(key('initialChar', 
  translate(substring(c:dyu,1,1),
  'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
  'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ'))[1])]">

  <!-- sort here -->
  <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending" />
  <xsl:variable name="myChar" select="translate(substring(c:dyu,1,1),
  'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
  'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

  <!-- output all the nodes having this intial letter value -->
  <xsl:for-each select="key('initialChar',$myChar)">
    <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending"/>

    <xsl:for-each select="c:trans">

      <xsl:for-each select="c:speech">
        <xsl:variable name="pos" select="c:type" />

        <xsl:choose>
          <xsl:when test="$pos='composé'">
               <xsl:variable name="id" select="substring(c:t-uuid, 1, 6)" />
               <xsl:variable name="link" select="translate(substring(ancestor::c:headword/c:dyu,1,2),
               'ɛɔàáɔ́ɔ̀ɛ́ɛ̀íì',
               'eoaaooeeii')" />
               <xsl:value-of select="c:def/c:note2" />
               <xsl:value-of select="$delim" />
               <xsl:value-of select="concat($link, $id)" />
                <xsl:value-of select="$newline" />
          </xsl:when>

          <xsl:otherwise>
               <xsl:variable name="id" select="substring(c:t-uuid, 1, 6)" />
               <xsl:variable name="link" select="translate(substring(ancestor::c:headword/c:dyu,1,2),
               'ɛɔàáɔ́ɔ̀ɛ́ɛ̀íì',
               'eoaaooeeii')" />
            <xsl:value-of select="ancestor::c:headword/c:dyu" />
            <xsl:value-of select="$delim" />
            <xsl:value-of select="concat($link, $id)" />
            <xsl:value-of select="$newline" />

            <xsl:if test="../../c:alt !=''">
              <xsl:for-each select="../../c:alt">
                <xsl:value-of select="." />
                <xsl:value-of select="$delim" />
                <xsl:value-of select="concat($link, $id)" />
                <xsl:value-of select="$newline" />
              </xsl:for-each>
            </xsl:if>

          </xsl:otherwise>
        </xsl:choose>

      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>

</xsl:for-each>
</xsl:template>

</xsl:stylesheet>

<!--
  <xsl:value-of select="concat(ancestor::c:headword/c:dyu, $id)" />
  <xsl:if test="ancestor::c:headword/c:alt !=''">
  <xsl:for-each select="ancestor::c:headword/c:alt">
  <xsl:value-of select="ancestor::c:headword/c:alt" />
  <xsl:value-of select="$delim" />
  <xsl:value-of select="concat(ancestor::c:headword/c:dyu, '-', $id)" />
  <xsl:value-of select="$newline" />
  </xsl:for-each>
  </xsl:if>
-->
