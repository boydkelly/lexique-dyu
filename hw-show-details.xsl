<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:coas="http://www.coastsystems.net"
xmlns:c="http://www.coastsystems.net"
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tdef" select="'|==='" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="right_bottom" select="'>.>| '" />
<xsl:param name="asterisk" select="'* '" />
<xsl:param name="level1" select="'== '" />
<xsl:param name="level2" select="'=== '" />
<xsl:param name="level3" select="'==== '" />
<xsl:param name="doublecolon" select="'::'" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:param name="delim" select="','" />
<xsl:strip-space elements="*" />

<xsl:key name="initialChar" match="c:lexique/c:headword" use="translate(substring(c:dyu,1,1),
'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

<xsl:template match="c:lexique">

  <!-- header -->
  <xsl:text>:leveloffset: -1</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:text>:page-partial:</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:text>[grid="rows",cols="20,5,10,30,5,30"]</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
  <xsl:text>|jula||pd|français||note</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />

  <!--
    #not neded for now
    genlinks not currently used; asciidoctor makes links in sections 
    <xsl:call-template name="genLinks"/>
  -->

    <xsl:value-of select="$newline" />
    <xsl:value-of select="$newline" />
    <!-- iterate over all the unique initial letter values -->
    <xsl:for-each select="//c:headword[generate-id(.)=generate-id(key('initialChar', 
    translate(substring(c:dyu,1,1),
    'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
    'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ'))[1])]">

    <!-- sort here -->
    <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending" />
    <xsl:variable name="myChar" select="translate(substring(c:dyu,1,1),
    'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
    'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

    <!-- output the level1 header  and start asciidoc table-->
    <xsl:value-of select="$newline" />
    <xsl:value-of select="concat($level2, string($myChar))" /> 
    <xsl:value-of select="$newline" />
    <xsl:value-of select="$newline" />
    <xsl:text>toc&#58;&#58;[]</xsl:text>

    <!--
      <xsl:text>&lt;&lt;here,Index&gt;&gt;</xsl:text>
      <xsl:variable name="id" select="substring(c:dyu/c:trans/c:speech/c:t-uuid, 1, 2)" />
      "<a href='#'><img class='icon' src='/images/icons/chevron-up.svg'></a>")"/>
    -->

      <xsl:value-of select="$newline" />
      <xsl:value-of select="$newline" />
      <xsl:text>[role="flex",grid="rows",cols="20,5,10,5,30,30"]</xsl:text>
      <xsl:value-of select="$newline" />
      <xsl:value-of select="$tdef" />
      <xsl:value-of select="$newline" />

      <!-- output all the nodes having this intial letter value -->
      <xsl:for-each select="key('initialChar',$myChar)">
        <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending"/>

        <xsl:value-of select="concat($asciidoctor, '[#', c:dyu,']' )" />
        <!-- we are in column 1 here -->
        <xsl:value-of select="$newline" />
        <xsl:if test="c:emp !=''">
          <xsl:value-of select="concat('*', c:dyu, '* ', '*_', c:emp, '_*'  )" />
        </xsl:if>
        <xsl:if test="c:emp =''">
          <xsl:value-of select="concat('*', c:dyu,'*' )" />
        </xsl:if>

        <xsl:if test="c:alt">
          <xsl:value-of select="$plus"/>
        </xsl:if>
        <xsl:value-of select="$newline" />

        <xsl:for-each select="c:alt">
          <xsl:value-of select="text()" />
          <xsl:value-of select="$plus"/>
          <xsl:value-of select="$newline" />
        </xsl:for-each>

        <xsl:if test="c:cf != ''">
          <xsl:text>cf: </xsl:text>
          <xsl:for-each select="c:cf">
            <xsl:value-of select="text()" />
            <xsl:if test="following-sibling::c:cf">
              <xsl:value-of select="$delim" />
            </xsl:if>
          </xsl:for-each>
        </xsl:if>

        <xsl:value-of select="$newline" />
        <xsl:for-each select="c:trans">
          <xsl:value-of select="c:detail" />

          <xsl:for-each select="c:speech">
            <xsl:variable name="pos" select="c:type" />

            <xsl:if test="position() = 1 ">
              <xsl:value-of select="$separator"/>
              <xsl:value-of select="$separator"/>
              <!-- we are in column 2 here -->
            </xsl:if>

            <xsl:if test="position() > 1 ">
              <!-- after the firt pos separator replaces the dyu-->
              <xsl:value-of select="$separator"/>
              <!-- Column 1  -->
              <xsl:value-of select="$separator"/>
              <!-- Column 2  -->
              <xsl:value-of select="$separator"/>
            </xsl:if>

            <!-- would be nice to have this before dyu at the beginning but with need th t-uuid here to make link-->
            <xsl:choose>
              <xsl:when test="$pos='composé'">
                <!-- do nothing as its already a deriviatave of an exsiting word that is alreay on the left...  ;)-->
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="c:type"></xsl:value-of>
                <xsl:variable name="id" select="substring(c:t-uuid, 1, 6)" />
                <xsl:variable name="link" select="translate(substring(ancestor::c:headword/c:dyu,1,2),
                'ɛɔàáɔ́ɔ̀ɛ́ɛ̀í',
                'eoaaooeei')" />
                <xsl:value-of select="concat(' [[', $link, $id,']] ')" />
              </xsl:otherwise>
            </xsl:choose>

            <!-- between the speech type and gloss-->
            <xsl:value-of select="concat(' 2+', $separator)"/>

            <xsl:for-each select="c:def">
              <xsl:if test="position() = 1 ">
                <xsl:if test="following-sibling::c:def">
                  <xsl:value-of select="concat(position(), '. ')" />
                </xsl:if>
                <xsl:value-of select="c:gloss"></xsl:value-of>
                <xsl:value-of select="$separator" />

                <xsl:choose>
                  <xsl:when test="$pos='composé'">
                    <xsl:variable name="id" select="substring(../c:t-uuid, 1, 6)" />
                    <xsl:variable name="link" select="translate(substring(ancestor::c:headword/c:dyu,1,2),
                    'ɛɔàáɔ́ɔ̀ɛ́ɛ̀í',
                    'eoaaooeei')" />
                    <xsl:value-of select="concat(' [[', $link, $id,']] ')" />
                    <xsl:value-of select="concat('*', c:note2,'*' )" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="c:note3"></xsl:value-of>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="$newline" />
              </xsl:if>

              <xsl:if test="position() > 1 ">
                <xsl:value-of select="$newline" />
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="$separator"/>
                <xsl:value-of select="concat(' 2+', $separator)"/>
                <xsl:value-of select="concat(position(), '. ')" />
                <xsl:value-of select="c:gloss"></xsl:value-of>
                <xsl:value-of select="$separator"/>
                <xsl:choose>
                  <xsl:when test="$pos='composé'">
                    <xsl:value-of select="concat('*', c:note2,'*' )" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="c:note3"></xsl:value-of>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="$newline" />
              </xsl:if>

              <xsl:for-each select="c:example">

                <xsl:if test="c:source != ''"> 
                  <xsl:if test="position() = 1 ">
                    <xsl:value-of select="$separator"/>
                    <xsl:value-of select="$separator"/>
                    <xsl:value-of select="$separator"/>
                    <xsl:value-of select="$separator"/>
                    <xsl:value-of select="concat(' 2+', $asciidoctor)"/>
                  </xsl:if>

                  <xsl:if test="position() > 0 ">
                    <xsl:value-of select="c:source"></xsl:value-of>
                    <xsl:value-of select="$doublecolon" />

                    <xsl:value-of select="$newline" />
                    <xsl:value-of select="c:target"></xsl:value-of>
                    <xsl:value-of select="$newline" />
                  </xsl:if>
                </xsl:if>

              </xsl:for-each>

            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:value-of select="$newline" />
      </xsl:for-each>
      <xsl:value-of select="$tdef"/>
      <xsl:value-of select="$newline" />
    </xsl:for-each>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <xsl:template name="genLinks">
    <!-- The for-each will iterate exactly once for each unique initial letter. -->
    <xsl:for-each select="//c:headword[generate-id(.)=generate-id(key('initialChar', 
    translate(substring(c:dyu,1,1),
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))[1])]">
    <xsl:sort select="c:dyu"/>
    <xsl:variable name="myLinkChar" select="translate(substring(c:dyu,1,1),
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <a><xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="$myLinkChar"/></xsl:attribute>
      <xsl:value-of select="$myLinkChar"/></a>
    <xsl:if test="position()!=last()"><xsl:text>-</xsl:text></xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
<!--
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$newline" />
-->
