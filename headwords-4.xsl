<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
xmlns:xs   = "http://www.w3.org/2001/XMLSchema"
xmlns:c    = "http://www.coastsystems.net">

  <!-- ouput tsv file of all glosses (repeats dyu for each gloss)-->
<xsl:output method="text" encoding="UTF-8" />

<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="'|'" />
<xsl:param name="CSEP" select="'&#09;'" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<xsl:template match="/">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="c:lexique/c:headword" />
  <xsl:variable name="defs" select="$headwords/c:trans/c:speech/c:def" />

  <!-- Determine the maximal number of <alt> <cf> and <example> elements -->

        <xsl:variable name="altCount">
          <xsl:for-each select="$headwords">
            <count count="{count(c:alt)}" />
          </xsl:for-each>
        </xsl:variable>
        <!-- hard code this to 4 <xsl:variable name="maxAlts" select="max($altCount/count/@count) cast as xs:integer" />-->
        <xsl:variable name="maxAlts" select="4 cast as xs:integer" />


        <xsl:variable name="cfCount">
          <xsl:for-each select="$headwords">
            <count count="{count(c:cf)}" />
          </xsl:for-each>
        </xsl:variable>
          <!-- hard code to 4 <xsl:variable name="maxCfs" select="max($cfCount/count/@count) cast as xs:integer" /> -->
          <xsl:variable name="maxCfs" select="4 cast as xs:integer" />

          <xsl:variable name="exampleCount">
            <xsl:for-each select="$defs">
              <count count="{count(c:example)}" />
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer" />

          <!-- Header. -->
          <xsl:if test="$header">
            <!-- dyu -->
            <xsl:text>dyu</xsl:text>
            <!-- alt1..altN -->
            <xsl:for-each select="(1 to $maxAlts)">
              <xsl:copy-of select="$CSEP" />
              <xsl:text>alt</xsl:text>
              <xsl:value-of select="." />
            </xsl:for-each>


            <!-- emp -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>emp</xsl:text>

            <!-- cf -->
            <xsl:for-each select="(1 to $maxCfs)">
              <xsl:copy-of select="$CSEP" />
              <xsl:text>cf</xsl:text>
              <xsl:value-of select="." />
            </xsl:for-each>

            <!-- lang -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>lang</xsl:text>

            <!-- detail -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>detail</xsl:text>
            <!-- type -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>type</xsl:text>
            <!-- gloss -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>gloss</xsl:text>
            <!-- note -->
            <xsl:copy-of select="$CSEP" />
            <xsl:text>note</xsl:text>
            <!-- source1,target1..sourceN,targetN -->
            <xsl:for-each select="(1 to $maxExamples)">

              <xsl:copy-of select="$CSEP" />
              <xsl:text>source</xsl:text>
              <xsl:value-of select="." />
              <xsl:copy-of select="$CSEP" />
              <xsl:text>target</xsl:text>
              <xsl:value-of select="." />
            </xsl:for-each>
            <xsl:copy-of select="$RSEP" />
          </xsl:if>

          <!-- Data -->
          <xsl:for-each select="$defs">
            <xsl:sort select="ancestor::c:headword/c:dyu" lang="en" />

            <!-- dyu -->
            <xsl:value-of select="ancestor::c:headword/c:dyu" />

            <!-- alt1..altN -->
            <xsl:variable name="alts" select="ancestor::c:headword/c:alt" />
            <xsl:for-each select="$alts">
              <xsl:copy-of select="$CSEP" />
              <xsl:value-of select="." />
            </xsl:for-each>
            <xsl:for-each  select="((count($alts) + 1) cast as xs:integer to $maxAlts)">
              <xsl:copy-of select="$CSEP" />
              <xsl:text></xsl:text>
            </xsl:for-each>

            <!-- emp -->
            <xsl:copy-of select="$CSEP" />
            <xsl:value-of select="ancestor::c:headword/c:emp" />

            <xsl:variable name="cfs" select="ancestor::c:headword/c:cf" />
            <xsl:for-each select="$cfs">
              <xsl:copy-of select="$CSEP" />
              <xsl:value-of select="." />
            </xsl:for-each>
            <xsl:for-each
            select="((count($cfs) + 1) cast as xs:integer to $maxCfs)">
            <xsl:copy-of select="$CSEP" />
            <xsl:text></xsl:text>
          </xsl:for-each>

          <!-- lang -->
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="ancestor::c:trans/c:lang" />

          <!-- detail -->
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="ancestor::c:trans/c:detail" />
          <!-- type -->
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="parent::c:speech/c:type" />
          <!-- gloss -->
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="c:gloss" />
          <!-- note -->
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="c:note2" />
          <!-- source1,target1..sourceN,targetN -->
          <xsl:variable name="examples" select="c:example" />
          <xsl:for-each select="$examples">
            <xsl:copy-of select="$CSEP" />
            <!-- Normalize space because the fields may have stray LFs. -->
            <xsl:value-of select="normalize-space(c:source)" />
            <xsl:copy-of select="$CSEP" />
            <xsl:value-of select="normalize-space(c:target)" />
          </xsl:for-each>
          <xsl:for-each select="((count($examples) + 1) cast as xs:integer to $maxExamples)">
            <xsl:copy-of select="$CSEP" />
            <xsl:text></xsl:text>
            <xsl:copy-of select="$CSEP" />
            <xsl:text></xsl:text>
          </xsl:for-each>
          <xsl:copy-of select="$RSEP" />
        </xsl:for-each>
      </xsl:template>

    </xsl:stylesheet>

