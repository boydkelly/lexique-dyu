#!/usr/bin/bash
sed -i "/^    trans:/i\    emp: null\n\    cf:\n\      - null" headwords.yaml
sed -i "/^                    target/a\                    ex-id:" headwords.yaml
sed -i "/^              - gloss:/a\                gl-id:" headwords.yaml
#added the gl-id with vim macro.  could/should have done below
sed -i -r "s/(ex-id:)/(printf '                    ex-id: '; uuidgen -r)/e" headwords.yaml
