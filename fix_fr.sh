#!/usr/bin/bash
# sed -i 's/ \»/\xC2\xA0»/g' $1
# sed -i 's/« /«\xC2\xA0/g' $1
# sed -i 's/ \!/\xC2\xA0\!/g' $1
# sed -i 's/ ?/\xC2\xA0?/g' $1
# 
# <u>  117,  Hex 75,  Octal 165 < ́> 769, Hex 0301, Octal 1401
# <ú> 250, Hex 00fa, Oct 372, Digr u'
# 
# <i>  105,  Hex 69,  Octal 151 < ́> 769, Hex 0301, Octal 1401
# <í> 237, Hex 00ed, Oct 355, Digr i'
# 
# <a>  97,  Hex 61,  Octal 141 < ́> 769, Hex 0301, Octal 1401
# <á> 225, Hex 00e1, Oct 341, Digr a'
# 
# <o>  111,  Hex 6f,  Octal 157 < ́> 769, Hex 0301, Octal 1401
# <ó> 243, Hex 00f3, Oct 363, Digr o'

#this was to replace some rogue i believe double unicode character with the single (normal) version that is found everywhere.
sed -i 's/\\x6F\\x0301/ó/g'
#sed 's/\\x0D/\\x0D \\x0A/g'

