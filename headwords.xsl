<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:coas="http://www.coastsystems.net"
xmlns:c="http://www.coastsystems.net"
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tdef" select="'|==='" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="asterisk" select="'* '" />
<xsl:param name="doublecolon" select="' ::'" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:strip-space elements="*" />


<xsl:template match="c:lexique">

  <!-- header -->
  <xsl:text>[grid="rows",cols="15,10,20,25,30"]</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
  <xsl:text>|jula|gram.|français|note|exemples</xsl:text>
  <xsl:value-of select="$newline" />

  <xsl:for-each select="c:headword">
    <xsl:sort select="c:dyu" lang="en" data-type="text" order="ascending"/>
    <xsl:value-of select="concat($asciidoctor, '*', c:dyu,'*')" />

    <xsl:if test="c:alt">
      <xsl:value-of select="$plus"/>
    </xsl:if>
    <xsl:value-of select="$newline" />

    <xsl:for-each select="c:alt">
      <xsl:value-of select="text()" />
      <xsl:value-of select="$plus"/>
      <xsl:value-of select="$newline" />
    </xsl:for-each>

    <xsl:for-each select="c:trans">
      <xsl:value-of select="c:detail" />
      <xsl:value-of select="$separator"/>

      <xsl:for-each select="c:speech">
        <xsl:value-of select="c:type"></xsl:value-of>
        <xsl:value-of select="$separator"/>

        <xsl:if test="position() > 1 ">
          <xsl:value-of select="$newline" />
          <xsl:value-of select="$separator"/>
          <xsl:value-of select="c:type"></xsl:value-of>
          <xsl:value-of select="$separator"/>
        </xsl:if>

        <xsl:for-each select="c:def">
          <xsl:if test="position() = 1 ">
            <xsl:value-of select="c:gloss"></xsl:value-of>
            <xsl:value-of select="$separator" />
            <xsl:value-of select="c:note"></xsl:value-of>
          </xsl:if>

          <xsl:if test="position() > 1 ">
            <xsl:value-of select="$newline" />
            <xsl:value-of select="$separator"/>
            <xsl:value-of select="$separator"/>
            <xsl:value-of select="$separator"/>
            <xsl:value-of select="c:gloss"></xsl:value-of>
            <xsl:value-of select="$separator"/>
            <xsl:value-of select="c:note"></xsl:value-of>
          </xsl:if>

          <xsl:for-each select="c:example">
            <xsl:value-of select="$newline" />

            <xsl:if test="position() = 1 ">
              <xsl:value-of select="$asciidoctor"/>
            </xsl:if>

            <xsl:value-of select="c:source"></xsl:value-of>
            <xsl:if test="c:source != ''"> 
              <xsl:value-of select="$doublecolon" />
            </xsl:if>
            <xsl:value-of select="$newline" />
            <xsl:value-of select="c:target"></xsl:value-of>

          </xsl:for-each>
          <xsl:value-of select="$newline" />

        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
    <xsl:value-of select="$newline" />
  </xsl:for-each>

  <xsl:value-of select="$tdef"/>
  <xsl:value-of select="$newline" />
</xsl:template>
</xsl:stylesheet>

