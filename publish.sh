#!/usr/bin/bash
set -o pipefail

branch=$1
me=$0

[ -z "$branch" ] && { echo "You need to specify a main or dev branch"; exit 1; }

[ "$branch" = "main" -o "$branch" = "dev" ] || { echo "You need to specify main or dev branch"; exit 1; }

for x in csvclean xslt3 asciidoctor ; do
  type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

set -o nounset
set -o errexit


function validate {
  csvclean -n -v -t $1
  awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
}

git -C ~/dev/web-docs checkout $1 || { echo "checkout $1 docs failed" ; exit 1 ; }

git -C ~/dev/web-docs branch --show-current

sh ./headwords2xml.sh
for transform in hide ; do
  project="lexique"
  version=dyu
  file=${project}-${version}-${transform}.adoc
  includefile=vocab-${version}-${transform}.adoc

  hw=headwords.yaml
  cp -v _lexique-dyu.adoc $file
  sed -i "s/transform/$transform/" $file

  #stats
  today=$(date -Im)
  headwords=$(grep dyu: $hw | wc -l)
  sub=$(grep composé $hw | wc -l)
  examples=$(sed -n -r '/source: [^\s*]/p' $hw | wc -l)

  echo $today,$headwords,$sub,$examples >> stats.csv
  sed -i "s/x_date/$(LANG=en_CA date -u '+%d %B %Y')/" $file 
  sed -i "s/x_words/$headwords/" $file
  sed -i "s/x_sub/$sub/" $file
  sed -i "s/x_ex/$examples/" $file

  xslt3 -xsl:hw-${transform}-details.xsl -s:headwords.xml -o:"${includefile}"
  asciidoctor -a includedir=./ -a skip-front-matter "${file}"

 if [[ "$transform" = "show" ]] ; then
  sed -i "s/^draft:.*/draft: true/" $file
 fi

 if [[ "$transform" = "hide" ]] ; then
  sed -i "/^:date:/a :noindex:" "$file"
 fi

  for x in web-docs ; do
    cp -v ${file} ~/dev/$x/docs/fr/modules/ROOT/pages/
    cp -v $includefile ~/dev/$x/docs/fr/modules/ROOT/partials/
    #cp $out ~/dev/web-prod/content/dyu/page/
  done
done

#xslt3 -xsl:headwords-4.xsl -s:headwords.xml -o:headwords-4.tsv
#xslt3 -xsl:hw-anki-words.xsl -s:headwords.xml -o:hw-anki-words.tsv
#xslt3 -xsl:hw-anki-phrases.xsl -s:headwords.xml -o:hw-anki-phrases.tsv
xslt3 -xsl:hw-codes.xsl -s:headwords.xml | sort -u -t, -k1,1 > dyu-codes.csv
validate dyu-codes.csv

#cp -v hw-anki-phrases.tsv ~/dev/jula/slides-dyu/
#cp -v hw-anki-phrases.tsv ~/Anki/brainbrew/Examples/src/data/Phrases.tsv
#cp -v hw-anki-words.tsv ~/Anki/brainbrew/Jula/src/data/
cp -v dyu-codes.csv ~/dev/web-docs/docs/fr/modules/proverbs/pages/
cp -v dyu-codes.csv ~/dev/web-docs/docs/fr/modules/cours/pages/
cp -v dyu-codes.csv ~/dev/web-docs/docs/dyu/modules/ROOT/pages/

echo $me
git -C ~/dev/web-docs/ commit -a -m "update by $me on $(date -I)"

sed -i -e 's/[ \t]*$//' headwords.yaml && sed -i '/^$/d' headwords.yaml

rm headwords.xml

#git commit -a -m $0 && git push --quiet origin main
#&& git push --quiet origin master

#pushd ~/dev/mandenkan-docs/pages/ && git add $out && git commit $out -m $out && git push --quiet origin master
#popd
#pushd ~/dev/web-prod/content/dyu/page/  && git add $out && git commit $out -m $out && git push --quiet origin master
